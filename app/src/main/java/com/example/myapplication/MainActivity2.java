package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity2 extends AppCompatActivity {
    String baseText;
    HTTPHandler handler;

    EditText message;
    TextView message0;
    TextView message1;
    TextView auth0;
    TextView auth1;
    TextView messages;
    Button send;
    JSONArray array;
    JSONObject object;
    String web_result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Выбор дизайна//
        setContentView(R.layout.activity_main2);
        //Создание или привязка переменных//
        handler=new HTTPHandler();
        message=findViewById(R.id.message);
        message1=findViewById(R.id.message1);
        message0=findViewById(R.id.message);
        auth0=findViewById(R.id.auth0);
        auth1=findViewById(R.id.auth1);
        messages=findViewById(R.id.messages);
        send= findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                sendMessage();
            }
        });
        getMessages();
    }

    public void getMessages() {
        messages.setText(handler.makeRequest("http://94.198.220.135:3000/get_messages"));
        try {
            array=new JSONArray(web_result);
            object=array.getJSONObject(0);
            auth0.setText(object.getString("student_name"));
            message0.setText(object.getString("message"));

            object=array.getJSONObject(1);
            auth1.setText(object.getString("student_name"));
            message1.setText(object.getString("message"));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void sendMessage() {
        baseText = handler.makeRequest("http://94.198.220.135:3000/insert_message?student_name=Jonona"
                 + "&message=" + message.getText());

        getMessages();
    }

}








