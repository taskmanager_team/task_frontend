//служебное название "пекеджа"
package com.example.myapplication;
//Подключение системных модулей для работы пустого приложения
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//Создаю пустое окно
public class MainActivity extends AppCompatActivity {

    @Override
    //Событие при создании/запуске окна
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void LoadContent(View v){
        HTTPHandler handler;
        handler=new HTTPHandler();

        String stroka;
        handler.makeRequest("http://94.198.220.135:3000/insert_student?student_name=Jonona");
        stroka=handler.makeRequest("http://94.198.220.135:3000/get_students");
        TextView label;
        label=findViewById(R.id.label);
        label.setText(stroka);
    }
}